package com.itau.projetobingo.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.projetobingo.model.Cartela;
import com.itau.projetobingo.model.Sorteio;
//controler
@RestController
public class ControllerBingo {
	
	@RequestMapping("/cartela")
	public Cartela getCartela() {

		Cartela cartela = new Cartela();
		
		cartela.preencherCartela();
		
		return cartela;
	}

	@RequestMapping("/sorteio")
	public Sorteio getSorteio() {
		Sorteio sorteio = new Sorteio();
		sorteio.preencherSorteio();
		return sorteio;
	}
	

	@RequestMapping(method = RequestMethod.POST, path = "/reiniciar")
	public ResponseEntity<Sorteio> reiniciarSorteio(@RequestBody Sorteio sorteio) {
		return ResponseEntity.status(203).body(sorteio = new Sorteio());
	}
}

